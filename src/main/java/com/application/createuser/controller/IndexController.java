package com.application.createuser.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("/") /* Request pin point alamat*/

public class IndexController {

    @GetMapping
    String index (Model x) {
        x.addAttribute("now", new Date());
        x.addAttribute("greetings","Welcome to the depth of your Self");
        return "index";
    }
}
