package com.application.createuser.domain;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {

    @NotNull
    @Size(min=2, max=10)
    private String userId;

    @NotNull
    @Size(min=4, max=30)
    private String userName;

    @NotNull
    private String gender;

    private Boolean freed;

    private String profile;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getFreed() {
        return freed;
    }

    public void setFreed(Boolean freed) {
        this.freed = freed;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}

